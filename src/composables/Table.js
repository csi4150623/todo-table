import { httpGet, httpPost, httpPut, httpDel } from "boot/axios";
import { ref, readonly } from "vue";
import { Search } from "./Search";

let TableDetails = ref(null);
/**
 * A request that is used to fetch all or numbers from the database
 *
 * @returns Array
 */
let SetTable = ref([]);
let GetTable = readonly(SetTable);

const FetchTable = () => {
  return new Promise((resolve, reject) => {
    httpGet(`Table`, {
      success(response) {
        // response.data.status === "success" &&
        //   (SetORNumbers.value = response.data);
        SetORNumbers.value = response.data;

        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

/**
 * A request that is used to fetch a specific or number from the database
 *
 * @returns Array
 */

// Fetching data by id from the table to update account type
const FetchTables = (orID) => {
  return new Promise((resolve, reject) => {
    httpGet(`Table/${orID}`, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

/**
 * A request that is used to insert a new range of or numbers to the database
 *
 * @param {*} payload
 * @returns Object
 */

const InsertNewRange = (payload) => {
  return new Promise((resolve, reject) => {
    httpPost("Table", payload, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

const InsertTable = (payload) => {
  return new Promise((resolve, reject) => {
    httpPost("Table", payload, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

/**
 * A request that is used to update a range of or numbers to the database
 *
 * @param {*} payload
 * @returns Object
 */
const UpdateTable = (payload) => {
  return new Promise((resolve, reject) => {
    console.log("composable :", payload);
    httpPut(`Table/${payload.id}`, payload, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

const DeleteTable = (payload) => {
  return new Promise((resolve, reject) => {
    httpDel(`Table/${payload.id}`, payload, {
      success(response) {
        if (response.data.status === "success") {
          let objectIndex = SetORNumbers.value.findIndex(
            (b) => b.id === payload.id
          );
          objectIndex !== -1 && SetORNumbers.value.splice(objectIndex, 1);
        }
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

/**
 * A request that is used to fetch all fund types from database
 *
 * @returns Array
 */
let SetFundTypes = ref([]);
let GetFundTypes = readonly(SetFundTypes);
const FetchFundType = () => {
  return new Promise((resolve, reject) => {
    httpGet(`polanguiTreasury/fundType`, {
      success(response) {
        response.data.status === "success" &&
          (SetFundTypes.value = response.data.data);

        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

/**
 * A request that is used to fetch all form types from database
 *
 * @returns Array
 */
let SetFormTypes = ref([]);
let GetFormTypes = readonly(SetFormTypes);
const FetchFormType = () => {
  return new Promise((resolve, reject) => {
    httpGet(`polanguiTreasury/formType`, {
      success(response) {
        response.data.status === "success" &&
          (SetFormTypes.value = response.data.data);

        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

export {
  GetTable,
  FetchTable,
  InsertNewRange,
  UpdateTable,
  DeleteTable,
  // ORNumbersSearchResult,
  FetchFundType,
  GetFundTypes,
  FetchFormType,
  GetFormTypes,
  FetchTables,
  InsertTable,
  TableDetails,
};
